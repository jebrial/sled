# Usage of sled

This document describes commands, flags, and environment variables for sled.

## Commands

`start` - Starts a server serving a proxy auto configuration file.

`version` - Displays the current version.

## Flags

`-h --help` - Displays the help screen.

## Environment Variables
