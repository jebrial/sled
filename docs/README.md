# Documentation

## Getting started

New users of sled should get started with [getting][download_build] sled from source. After getting sled built, reading the `Using sled` section is a great resource to get sled up and running.

## Developing sled

_Coming Soon_

## Using sled

- [Binary Usage and Variables][usage]
- [Running sled][running]

[usage]: usage.md
[running]: running.md
[download_build]: dl_build.md
